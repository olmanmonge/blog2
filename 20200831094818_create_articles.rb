class CreateArticles < ActiveRecord::Migration[6.0]
  def change
    create_table :articles do |t|
      t.string :body
      t.text :title

      t.timestamps
    end
  end
end
